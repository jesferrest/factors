<?php

include("functions.php");

if(isset($_POST['number']) && is_numeric($_POST['number']) && $_POST['number'] > 1){
    $factors = array();
    $aux = $_POST['number'];
    
    while($aux > 1){
        $divisible = false;
        $number = 1;
        while(!$divisible){
            $number++;
            if(prime($number) && 0 == $aux % $number){
                $divisible = true;
            }
        }
        $aux = $aux / $number;
        array_push($factors, $number);
    }
    
    $divisors = '';
    foreach($factors as $value){
        $divisors .= $value.',';
    }
    $divisors = trim($divisors, ',');
}
else{
    header("location: index.html");
    exit();
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Factors</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/reset.css" />
        <link rel="stylesheet" href="./css/style.css" />
    </head>
    <body>
        <div id="content">
            <span class="text1">The result is <?php echo $divisors; ?></span>
            <br /><br />
            <a href="index.html" >Enter new number</a>
        </div>
    </body>
</html>
